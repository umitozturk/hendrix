class Artist < ApplicationRecord
  has_many :records, dependent: :destroy
end
